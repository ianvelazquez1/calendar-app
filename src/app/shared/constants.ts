export const CONSTATS = {
    api_key:'14c10f076b9b15630f17d8f7ac2d8c6c',
    loc_storage_reminders:'reminders',
    cities_list:[
        {
            "id": 3513966,
            "name": "Zumpango",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.101669,
                "lat": 19.797779
            }
        },
        {
            "id": 3513967,
            "name": "Zumpango del Río",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.5,
                "lat": 17.65
            }
        },
        {
            "id": 3513969,
            "name": "Zumpahuacán",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.550003,
                "lat": 18.9
            }
        },
        {
            "id": 3513976,
            "name": "Zozocolco de Hidalgo",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.583328,
                "lat": 20.15
            }
        },
        {
            "id": 3513977,
            "name": "Zozocolco de Guerrero",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.566673,
                "lat": 20.116671
            }
        },
        {
            "id": 3514007,
            "name": "Zongozotla",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.73333,
                "lat": 19.98333
            }
        },
        {
            "id": 3514008,
            "name": "Zongolica",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -96.98333,
                "lat": 18.66667
            }
        },
        {
            "id": 3514013,
            "name": "Santa María Zolotepec",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.494438,
                "lat": 19.415831
            }
        },
        {
            "id": 3514021,
            "name": "Zitlala",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.083328,
                "lat": 17.633329
            }
        },
        {
            "id": 3514026,
            "name": "Zinacantán",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -92.699997,
                "lat": 16.75
            }
        },
        {
            "id": 3514027,
            "name": "Zimatlán de Álvarez",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -96.783333,
                "lat": 16.866671
            }
        },
        {
            "id": 3514031,
            "name": "Zimapan",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.349998,
                "lat": 20.75
            }
        },
        {
            "id": 3514043,
            "name": "Zempoala",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -98.666672,
                "lat": 19.91667
            }
        },
        {
            "id": 3514044,
            "name": "Villa Zempoala",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -96.416672,
                "lat": 19.450001
            }
        },
        {
            "id": 3514049,
            "name": "Zaragoza",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.716667,
                "lat": 21.283331
            }
        },
        {
            "id": 3514051,
            "name": "Zaragoza",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.550003,
                "lat": 19.76667
            }
        },
        {
            "id": 3514052,
            "name": "Zaragoza de Guadalupe",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.647217,
                "lat": 19.14583
            }
        },
        {
            "id": 3514060,
            "name": "Zapotlán de Juárez",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -98.849998,
                "lat": 19.966669
            }
        },
        {
            "id": 3514066,
            "name": "Zapotitlán del Río",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.23333,
                "lat": 16.883329
            }
        },
        {
            "id": 3514134,
            "name": "Zacualtipán",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -98.599998,
                "lat": 20.65
            }
        },
        {
            "id": 3514138,
            "name": "Zacualpan",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.783333,
                "lat": 18.716669
            }
        },
        {
            "id": 3514148,
            "name": "Zacatlán",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.966667,
                "lat": 19.933331
            }
        },
        {
            "id": 3514162,
            "name": "Zacatepec",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.533333,
                "lat": 19.26667
            }
        },
        {
            "id": 3514163,
            "name": "Zacatepec",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.199997,
                "lat": 18.65
            }
        },
        {
            "id": 3514179,
            "name": "Zacapoaxtla",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.583328,
                "lat": 19.883329
            }
        },
        {
            "id": 3514207,
            "name": "San Francisco Yucucundo",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.183327,
                "lat": 16.866671
            }
        },
        {
            "id": 3514211,
            "name": "Estado de Yucatán",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -89.0,
                "lat": 20.83333
            }
        },
        {
            "id": 3514272,
            "name": "Yehualtepec",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.650002,
                "lat": 18.783331
            }
        },
        {
            "id": 3514277,
            "name": "Yecuatla",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -96.75,
                "lat": 19.866671
            }
        },
        {
            "id": 3514278,
            "name": "Yecapixtla",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -98.866669,
                "lat": 18.883329
            }
        },
        {
            "id": 3514293,
            "name": "Yaxcopoil",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -89.466667,
                "lat": 20.75
            }
        },
        {
            "id": 3514297,
            "name": "Yaxchoj",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -92.133331,
                "lat": 16.25
            }
        },
        {
            "id": 3514321,
            "name": "Yautepec",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -99.067146,
                "lat": 18.88188
            }
        },
        {
            "id": 3514326,
            "name": "Yaonahuac",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -97.449997,
                "lat": 19.866671
            }
        },
        {
            "id": 3514329,
            "name": "Yanga",
            "state": "",
            "country": "MX",
            "coord": {
                "lon": -96.800003,
                "lat": 18.83333
            }
        },
    ]
}