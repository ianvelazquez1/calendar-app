import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() title;
  @Output() modalClosedEvent:EventEmitter<any> = new EventEmitter();
  modalHasOpened:boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  showModal(isShowing){
    let modalCont = document.querySelector('#modalCont') as HTMLElement;
    modalCont.classList.remove('invisibleModal');
    modalCont.classList.add('visibleModal');
    modalCont.classList.add('animate__animated','animate__fadeInUpBig');
    modalCont.addEventListener('animationend',(e)=> {
     if(isShowing){
     
      modalCont.classList.remove('animate__animated','animate__fadeInUpBig')
     }
    })

  }

  hideModal(isHiding){
    let modalCont = document.querySelector('#modalCont') as HTMLElement;
    this.modalHasOpened = false;
    modalCont.classList.add('invisibleModal');
  
  }

}
