import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EventsFormComponent } from './events-form.component';
import {FormsModule, NgForm, ReactiveFormsModule} from '@angular/forms';
import {WeatherService} from '../../services/weather.service';
import { ModalComponent } from '../../shared/modal/modal.component';
import { ViewChild } from '@angular/core';

describe('EventsFormComponent', () => {

  let formComponent:EventsFormComponent;
  let fixture: ComponentFixture<EventsFormComponent>;
  let spyWeatherService = jasmine.createSpyObj('weatherService',['queryWeather']);

  let  spyModalComponent = jasmine.createSpyObj('ModalComponent', ['hideModal']);


  spyWeatherService.queryWeather = () => {
    return []
  }

  spyModalComponent.hideModal = () => {
    return true;
  }

  beforeEach(async(()=> {
    TestBed.configureTestingModule({
      declarations:[EventsFormComponent,ModalComponent],
      imports:[FormsModule, ReactiveFormsModule],
      providers:[
      EventsFormComponent,
        {
        provide:WeatherService, useValue:spyWeatherService
      }]
    }).compileComponents()
    
    fixture = TestBed.createComponent(EventsFormComponent);
    formComponent = TestBed.inject(EventsFormComponent);
  }));

  it('should create', ()=> {
    
    expect(fixture).toBeTruthy();
  })

  it('Title should be Add reminder as default', async(()=> {
    expect(formComponent.title).toBe('Add Reminder');
  }));

  it('text should be less or equal to 30 characters', async(()=> {
    let textArea = (<HTMLInputElement>document.getElementById('subjectInput'));
      textArea.value = "1234567890";
      textArea.value = "123456789012345678912345678912";
      expect(textArea.value.length <= 30).toBe(true);
  }));

  it('Form required info should be full before creating an appointment', async(()=> {
    
    
    formComponent.reminderForm.date = '2020-03-08';
    formComponent.reminderForm.text ='Example text';
    formComponent.reminderForm.time ='14:25'

    expect(formComponent.checkRequiredInfo()).toBeTruthy();

  }));

  it('should emit an event for saving the appointment when the form is submited', async(()=> {


    formComponent.reminderForm.date = '2020-03-08';
    formComponent.reminderForm.text ='Example text';
    formComponent.reminderForm.time ='14:25'
    formComponent.reminderForm.color = "#fffff",
    formComponent.reminderForm.city =2,
    formComponent.reminderForm.weather = {};
    formComponent.reminderForm.subject = "";
    formComponent.isEditing = false;
    spyOn(formComponent.saveReminderEvent,'emit');

    formComponent.formModal = spyModalComponent;
    
    formComponent.saveReminder();


    expect(formComponent.saveReminderEvent.emit).toHaveBeenCalled();
  }))

});
