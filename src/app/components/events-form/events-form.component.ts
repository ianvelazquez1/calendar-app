import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ModalComponent } from '../../shared/modal/modal.component';
import { Day } from '../../types/Day';
import { Reminder } from '../../types/Reminder';
import { CONSTATS } from '../../shared/constants';
import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'app-events-form',
  templateUrl: './events-form.component.html',
  styleUrls: ['./events-form.component.css']
})
export class EventsFormComponent implements OnInit, OnChanges {

  @ViewChild(ModalComponent) formModal: ModalComponent;
  @Input() selectedDay: Day = new Day();
  @Input() reminderForm: Reminder = new Reminder();
  citiesList: any[] = [];
  gotCityWeather: boolean = false;
  text: string = "";
  @Output() saveReminderEvent: EventEmitter<Reminder> = new EventEmitter();
  @Output() editReminderEvent: EventEmitter<Reminder> = new EventEmitter();
  @Output() deleteReminderEvent: EventEmitter<any> = new EventEmitter();
  isEditing: boolean = false;
  title: string = "Add Reminder";

  constructor(private weatherService: WeatherService) {
    this.citiesList = CONSTATS.cities_list;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['reminderForm']) {
      console.log(this.reminderForm);
      if (Object.keys(this.reminderForm.weather).length > 0) {
        this.gotCityWeather = true;
      }
    }
  }

  ngOnInit(): void {

  }

  openModalEdition(reminder) {
    this.formModal.showModal(true);
    this.isEditing = true;
    this.title = 'Edit reminder';
    this.reminderForm = reminder;
    this.selectedDay.date = reminder.date;
    if (Object.keys(this.reminderForm.weather).length > 0) {
      this.gotCityWeather = true;
    } else {
      this.gotCityWeather = false;
    }
  }

  openModal() {
    this.formModal.showModal(true);
    this.reminderForm = new Reminder();
    this.gotCityWeather = false;
    this.isEditing = false;
  }

  hideModal() {
    this.formModal.hideModal(true);
  }

  queryForCityWeather() {
    console.log(this.reminderForm.city);
    this.weatherService.queryWeather(this.reminderForm.city, 16).subscribe(response => {
      debugger
      if (response['cod'] === 200) {
        this.reminderForm.weather = response['weather'][0];
        this.reminderForm.weather['temperature'] = response['main'].temp;
        this.gotCityWeather = true;
        this.transformToCelcius();
      }
    });
    debugger

  }

  transformToCelcius() {
    if (this.reminderForm.weather.temperature && this.reminderForm.weather.temperature != '') {
      this.reminderForm.weather.temperature = (this.reminderForm.weather.temperature - 273.15).toFixed(2);
    }
  }

  checkRequiredInfo() {
    return this.reminderForm.date != '' && this.reminderForm.text != '' && this.reminderForm.time != '';
  }

  saveReminder() {
    console.log(this.reminderForm);
    this.reminderForm.date = this.selectedDay.date;
  
      if (!this.isEditing) {
        this.saveReminderEvent.emit(this.reminderForm);
      } else {
        this.editReminderEvent.emit(this.reminderForm);
      }
    


    this.reminderForm = new Reminder();
    this.hideModal();
    this.gotCityWeather = false;
  }


  deleteReminder(){

    console.log(this.reminderForm);
    debugger
    
    this.deleteReminderEvent.emit(this.reminderForm.id);

    this.reminderForm = new Reminder();
    this.hideModal();
    this.isEditing = false;
    this.gotCityWeather = false;
  }

}
