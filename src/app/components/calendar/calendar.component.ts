import { Component, OnInit, ViewChild } from '@angular/core';
import { MonthsOfYear } from '../../types/MonthsOfYear';
import { Day } from '../../types/Day';
import { EventsFormComponent } from '../events-form/events-form.component';
import { CONSTATS } from '../../shared/constants';
import { Reminder } from '../../types/Reminder';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  @ViewChild(EventsFormComponent) eventFormComponent: EventsFormComponent;
  selectedReminder: Reminder = new Reminder();

  constructor() { }

  monthsOfYear: MonthsOfYear = new MonthsOfYear();
  minYear: number = 2000;
  maxYear: number = 2021;
  yearsArray: number[] = [];

  selectedYear: number;
  selectedMonth: any;
  selectedDay: Day = new Day();

  currentDaysInMonth: any[] = [];
  emptyDays: any[] = [];

  ngOnInit(): void {
    this.initializeYearsArray();
    console.log(this.monthsOfYear.months);
    let currentMonthIndex = this.monthsOfYear.months.findIndex(item => item.value === new Date().getMonth() + 1);
    if (currentMonthIndex != -1) {
      this.selectedMonth = this.monthsOfYear.months[currentMonthIndex];
      this.selectedYear = new Date().getFullYear();
      this.getPeriodSelectedDays()
    }
    this.selectedYear = new Date().getFullYear();

  }

  initializeYearsArray() {
    for (let i = this.minYear; i <= this.maxYear; i++) {
      this.yearsArray.push(i);
    }
  }

  ////SET DATES VALUES

  setCurrentMonthValue(value) {
    this.selectedMonth = value;
    this.openMonthsYearCont('monthsCont')
    this.getPeriodSelectedDays();
  }

  setCurrentYearValue(year) {
    this.selectedYear = year;
    this.openMonthsYearCont('yearsCont')
    this.getPeriodSelectedDays();
  }


  openMonthsYearCont(target) {

    let monthsYearsContainer = document.querySelector(`#${target}`) as HTMLElement;
    monthsYearsContainer.classList.toggle('visible');
    monthsYearsContainer.classList.add('animate__animated', 'animate__fadeIn', 'animate__faster');
    monthsYearsContainer.addEventListener('animationend', () => {
      monthsYearsContainer.classList.remove('animate__animated', 'animate__fadeIn', 'animate__faster');
    });
  }

  getDaysInMonth() {
    let date = new Date(this.selectedYear, this.selectedMonth.value, 0);
    return date.getDate();
  }


  getPeriodSelectedDays() {
    this.currentDaysInMonth = [];
    this.emptyDays = [];
    let todayDate = new Date(this.selectedYear, this.selectedMonth.value - 1, 0);
    let numberOfDaysInMonth = this.getDaysInMonth();
    let currentDay = todayDate.getDay();
    for (let i = 0; i < numberOfDaysInMonth; i++) {
      let objDay: Day = new Day();
      objDay.tag = String(i + 1);
      objDay.value = i;
      objDay.id = `${this.selectedYear}-${this.selectedMonth.value}-${i + 1}`;
      objDay.date = `${this.selectedYear}-${this.selectedMonth.value}-${i + 1}`;
      objDay['reminders'] = [];
      if (i <= currentDay) {
        objDay.is_visible = false;
        this.emptyDays.push(objDay);
      }
      this.currentDaysInMonth.push(objDay);
    }
    this.assingAppointMentsToDate();
  }

  assingAppointMentsToDate() {
    let savedAppointments = JSON.parse(localStorage.getItem(CONSTATS.loc_storage_reminders));
    if (savedAppointments && savedAppointments.length > 0) {
      this.currentDaysInMonth.map(element => {
        for (let appointment of savedAppointments) {
          if (element.date === appointment.date) {
            element['reminders'].push(appointment);
          }
        }

        if(element.reminders.length > 0){
         element['reminders'] = element['reminders'].sort((a,b) => {
            let timeSplitA = Number(a.time.split(':')[0]);
            let timeSplitB = Number(b.time.split(':')[0]);
            if(timeSplitA > timeSplitB){
              return 1;
            }
            if(timeSplitA < timeSplitB){
              return -1;
            }
            return 0;
          })
        }
        

        return element;
      });

    }
  }

  ///////FORM REMINDERS

  openReminderForm(day) {
    this.eventFormComponent.openModal();
    this.selectedDay = day;

  }

  closeReminderForm() {
    this.eventFormComponent.hideModal();
  }

  showAddBtn(index) {
    let btnElement = document.querySelector(`#rem-btn-${index}`) as HTMLElement;
    let btnContainerElement = document.querySelector(`#rem-container-${index}`) as HTMLElement;
    let miniBtnContainer = document.querySelector(`#mini-rem-container-${index}`) as HTMLElement;
    btnElement.classList.add('visible');
    miniBtnContainer.classList.add('invisible');
    btnContainerElement.classList.add('visible');
  }

  hideAddBtn(index) {
    let btnElement = document.querySelector(`#rem-btn-${index}`) as HTMLElement;
    let btnContainerElement = document.querySelector(`#rem-container-${index}`) as HTMLElement;
    let miniBtnContainer = document.querySelector(`#mini-rem-container-${index}`) as HTMLElement;

    btnContainerElement.classList.remove('visible');
    miniBtnContainer.classList.remove('invisible');
    btnElement.classList.remove('visible');
  }

  saveReminder(event) {
    let formEventArr = []
    if (localStorage.getItem(CONSTATS.loc_storage_reminders)) {
      formEventArr = JSON.parse(localStorage.getItem(CONSTATS.loc_storage_reminders))
      event['id'] = `${formEventArr.length}-${event.date}-${new Date().getMilliseconds()}`;
      formEventArr.push(event);
      localStorage.setItem(CONSTATS.loc_storage_reminders, JSON.stringify(formEventArr));
    } else {

      event['id'] = `0-${event.date}-${new Date().getMilliseconds()}`;
      formEventArr = [event]
      localStorage.setItem(CONSTATS.loc_storage_reminders, JSON.stringify(formEventArr));
    }
    this.getPeriodSelectedDays();
    return formEventArr.length;
  }

  getHour(reminder) {
    let time = reminder.time;
    let suffixTimeMain = time.split(':');
    let timeNum = Number(suffixTimeMain[0]);
    return `${reminder.time} ${timeNum >= 12 ? 'PM' : 'AM'}`;
  }

  editFormItem(reminder) {
    this.eventFormComponent.openModalEdition(reminder);
  }

  editReminder(reminder) {
    let formEventArr = JSON.parse(localStorage.getItem(CONSTATS.loc_storage_reminders));
    let indexItem = formEventArr.findIndex(item => item.id === reminder.id);
    if (indexItem != -1) {
      formEventArr[indexItem] = reminder;
    }
    localStorage.setItem(CONSTATS.loc_storage_reminders, JSON.stringify(formEventArr));
    this.getPeriodSelectedDays();
    debugger
  }

  deleteReminderEvent(id){
    let formEventArr = JSON.parse(localStorage.getItem(CONSTATS.loc_storage_reminders));
    let indexItem = formEventArr.findIndex(item => item.id === id);
    if (indexItem != -1) {
      formEventArr.splice(indexItem,1);
    }
    localStorage.setItem(CONSTATS.loc_storage_reminders, JSON.stringify(formEventArr));
    this.getPeriodSelectedDays();
  }

  deleteAllReminders(){
    localStorage.clear();
    this.getPeriodSelectedDays();
  }

}
