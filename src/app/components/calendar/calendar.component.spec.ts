import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarComponent } from './calendar.component';
import { CONSTATS } from '../../shared/constants';
import { Reminder } from '../../types/Reminder';

describe('CalendarComponent', () => {
  let component: CalendarComponent;
  let fixture: ComponentFixture<CalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save an appointment in local storage', async () => {
    let reminder: Reminder = {
      city: 3513966,
      color: '#fff',
      date: '2020-8-27',
      text: 'Test',
      subject: '',
      time: '12:12',
      weather: {}
    }
    if (localStorage.getItem(CONSTATS.loc_storage_reminders)) {
      let numberOfElements = component.saveReminder(reminder);
      expect(numberOfElements > 1).toBeTruthy();
    }else{
      let numberOfElements = component.saveReminder(reminder);
      expect(numberOfElements > 0 == numberOfElements <2).toBeTruthy();;
    }
  });


});
