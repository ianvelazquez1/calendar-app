export class Reminder {
    id?:string;
    date:string;
    time:string;
    subject:string;
    text:string;
    color:string;
    city:any;
    weather:any;
    constructor(){
        this.date ="";
        this.time ="";
        this.subject ="";
        this.text = "";
        this.color ="#00bcd4";
        this.city ="";
        this.weather = {}
    }
}