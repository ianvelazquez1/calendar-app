import {Reminder} from './Reminder';
export class Day {
    value:number;
    tag:string;
    reminders:Reminder[];
    city:string;
    is_visible:boolean;
    date:string;
    id?:string;
    constructor(){
        this.tag = "";
        this.reminders = [];
        this.city = "";
        this.is_visible = true;
        this.date = "";
    }
}