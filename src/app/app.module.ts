import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

//CALENDAR COMPONENTS
import {CalendarComponent} from './components/calendar/calendar.component';
import {EventsFormComponent} from './components/events-form/events-form.component';
import { ModalComponent } from './shared/modal/modal.component';

////SERVICES
import {WeatherService} from './services/weather.service';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    EventsFormComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
