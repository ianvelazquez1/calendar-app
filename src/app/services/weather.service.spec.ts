import { TestBed } from '@angular/core/testing';

import { WeatherService } from './weather.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('WeatherService', () => {
  let service: WeatherService;

  beforeEach(() => {
    TestBed.configureTestingModule({providers:[WeatherService], imports:[HttpClientModule]});
    TestBed.inject(HttpClient)
    service = TestBed.inject(WeatherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
