import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CONSTATS} from '../shared/constants';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private httpClient:HttpClient) { }

  queryWeather(cityId,numberOfDays){
    return this.httpClient.get(`https://api.openweathermap.org/data/2.5/weather?id=${cityId}&appid=${CONSTATS.api_key}
    `)
  }

}
